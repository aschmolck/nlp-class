import sys
import os
import re
import pprint

POS = r"""<script type="text/javascript">obfuscate('stanford.edu','jurafsky')</script>
jurafsky(at)cs.stanford.edu
jurafsky at csli dot stanford dot edu
jurafsky@stanford.edu
TEL +1-650-723-0291
Phone:  (650) 723-0292
Tel (+1): 650-723-0293
+1&thinsp;650&thinsp;723&thinsp;0294
hager at cs dot jhu dot edu
engler WHERE stanford DOM edu
support at gradiance dt com
Email: ashishg @ stanford.edu<o:p></o:p><br>
<a href='mail&#116;o&#58;Jo&#37;&#54;8n&#46;%44o&#101;&#64;e&#120;&#37;61%6Dpl&#101;&#46;&#99;%6Fm'>J&#111;hn&#46;D&#111;e&#64;exa&#109;&#112;&#108;e&#46;&#99;om</a>
<a href='mailt&#111;&#58;Jo%&#50;ED&#111;&#37;65343&#64;&#101;%78ample&#46;c&#111;m'>John&#46;Doe343&#64;ex&#97;m&#112;le&#46;com</a>
<script>document.write('<'+'a'+' '+'h'+'r'+'e'+'f'+'='+"'"+'m'+'a'+'i'+'l'+'t'+'&'+'#'+'1'+'1'+'1'+';'+'&'+'#'+'5'+'8'+';'+'J'+'o'+'e'+'%'+'&'+'#'+'5'+'0'+';'+'E'+'D'+'&'+'#'+'1'+'1'+'1'+';'+'&'+'#'+'3'+'7'+';'+'6'+'5'+'&'+'#'+'6'+'4'+';'+'&'+'#'+'1'+'0'+'1'+';'+'%'+'7'+'8'+'a'+'m'+'p'+'l'+'e'+'&'+'#'+'4'+'6'+';'+'c'+'&'+'#'+'1'+'1'+'1'+';'+'m'+"'"+'>'+'J'+'o'+'h'+'n'+'&'+'#'+'4'+'6'+';'+'D'+'o'+'e'+'&'+'#'+'6'+'4'+';'+'e'+'x'+'&'+'#'+'9'+'7'+';'+'m'+'&'+'#'+'1'+'1'+'2'+';'+'l'+'e'+'&'+'#'+'4'+'6'+';'+'c'+'o'+'m'+'<'+'/'+'a'+'>');</script><noscript>[Turn on JavaScript to see the email address]</noscript>
test1 {at} example {dot} com
test2ATexampleDOTcom
test3 at example dot edu
test4 "at" example =d0t= biz
test5N0_SPAM@example.com
test6@exampleN*0*S*P*A*M.com
test77@example.com.invalid
t e s t 7 @ f o o . c o m
test8<i>@</i>example<i>.</i>com
test9@@example.com
mailto:test10@example.com
<script type="text/javascript"> fromCharCode(109,97,105,108,116,111,58,116,101,115,116,49,49,64,101,120,97,109,112,108,101,46,99,111,109);</script>""".split('\n') #"
ANS = """jurafsky@stanford.edu
jurafsky@cs.stanford.edu
jurafsky@csli.stanford.edu
jurafsky@stanford.edu
650-723-0291
650-723-0292
650-723-0293
650-723-0294
hager@cs.jhu.edu
engler@stanford.edu
support@gradiance.com
ashishg@stanford.edu
john.doe@example.com
john.doe343@example.com
joe.doe@example.com
test1@example.com
test2@example.com
test3@example.edu
test4@example.biz
test5@example.com
test6@example.com
test77@example.com
test7@foo.com
test8@example.com
test9@example.com
test10@example.com
test11@example.com""".split("\n")
assert len(ANS) == len(POS)


NEG = """laughing at polka-dot commercials
Quick attack. Organize resistance.
server at standford.edu 880
apache at standford.edu 880
""".split('\n')

TOPLEVEL_DOMAINS='''
AC
AD
AE
AERO
AF
AG
AI
AL
AM
AN
AO
AQ
AR
ARPA
AS
ASIA
AT
AU
AW
AX
AZ
BA
BB
BD
BE
BF
BG
BH
BI
BIZ
BJ
BM
BN
BO
BR
BS
BT
BV
BW
BY
BZ
CA
CAT
CC
CD
CF
CG
CH
CI
CK
CL
CM
CN
CO
COM
COOP
CR
CU
CV
CW
CX
CY
CZ
DE
DJ
DK
DM
DO
DZ
EC
EDU
EE
EG
ER
ES
ET
EU
FI
FJ
FK
FM
FO
FR
GA
GB
GD
GE
GF
GG
GH
GI
GL
GM
GN
GOV
GP
GQ
GR
GS
GT
GU
GW
GY
HK
HM
HN
HR
HT
HU
ID
IE
IL
IM
IN
INFO
INT
IO
IQ
IR
IS
IT
JE
JM
JO
JOBS
JP
KE
KG
KH
KI
KM
KN
KP
KR
KW
KY
KZ
LA
LB
LC
LI
LK
LR
LS
LT
LU
LV
LY
MA
MC
MD
ME
MG
MH
MIL
MK
ML
MM
MN
MO
MOBI
MP
MQ
MR
MS
MT
MU
MUSEUM
MV
MW
MX
MY
MZ
NA
NAME
NC
NE
NET
NF
NG
NI
NL
NO
NP
NR
NU
NZ
OM
ORG
PA
PE
PF
PG
PH
PK
PL
PM
PN
PR
PRO
PS
PT
PW
PY
QA
RE
RO
RS
RU
RW
SA
SB
SC
SD
SE
SG
SH
SI
SJ
SK
SL
SM
SN
SO
SR
ST
SU
SV
SX
SY
SZ
TC
TD
TEL
TF
TG
TH
TJ
TK
TL
TM
TN
TO
TP
TR
TRAVEL
TT
TV
TW
TZ
UA
UG
UK
US
UY
UZ
VA
VC
VE
VG
VI
VN
VU
WF
WS
XN--0ZWM56D
XN--11B5BS3A9AJ6G
XN--3E0B707E
XN--45BRJ9C
XN--80AKHBYKNJ4F
XN--80AO21A
XN--90A3AC
XN--9T4B11YI5A
XN--CLCHC0EA0B2G2A9GCD
XN--DEBA0AD
XN--FIQS8S
XN--FIQZ9S
XN--FPCRJ9C3D
XN--FZC2C9E2C
XN--G6W251D
XN--GECRJ9C
XN--H2BRJ9C
XN--HGBK6AJ7F53BBA
XN--HLCJ6AYA9ESC7A
XN--J6W193G
XN--JXALPDLP
XN--KGBECHTV
XN--KPRW13D
XN--KPRY57D
XN--LGBBAT1AD8J
XN--MGBAAM7A8H
XN--MGBAYH7GPA
XN--MGBBH1A71E
XN--MGBC0A9AZCG
XN--MGBERP4A5D4AR
XN--O3CW4H
XN--OGBPF8FL
XN--P1AI
XN--PGBS0DH
XN--S9BRJ9C
XN--WGBH1C
XN--WGBL6A
XN--XKC2AL3HYE2A
XN--XKC2DL3A5EE0H
XN--YFRO4I67O
XN--YGBI2AMMX
XN--ZCKZAH
XXX
YE
YT
ZA
ZM
ZW'''.lower().split()
# FIXME: this reduce FPR but should really only be applied to degraded email addresses
TOPLEVEL_DOMAINS.remove('in')
TOPLEVEL_DOMAINS.remove('is')
TOPLEVEL_DOMAINS.remove('by')
TOPLEVEL_DOMAINS.remove('at')

TOPLEVELREX = "|".join(map(re.escape, TOPLEVEL_DOMAINS))

## EMAIL_FIXUP = re.compile(r'(?i)\b([\w.]+)[\(\{\[\"\' ]+(?:at|where)[\)\}\]\"\' ]+(?:([a-z]\w+) ){1,5}(?:%s)\b' % TOPLEVELREX)
EMAIL_FIXUP = re.compile(r'(?i)\b([\w.]+)\s*@\s*(?:([a-z]\w+) ){1,5}(?:%s)\b' % TOPLEVELREX)

my_first_pat = re.compile(r'(?i)\b([\w\.-]+)(?:\s+.{,3}followed by.*)?\s*@\s*([a-z]\w+(?:[;.]\w+)?)[;\.](%s)\b' % TOPLEVELREX)
#my_first_path = '
NA_PHONE=re.compile(r'(?<![\d\-])(?:\+1)?\s*[(-]?([2-9]\d{2})[)-]?\s*([2-9]\d{2})[ -]?(\d{4})(?![\d-])')



"""
TODO
This function takes in a filename along with the file object (actually
a StringIO object at submission time) and
scans its contents against regex patterns. It returns a list of
(filename, type, value) tuples where type is either an 'e' or a 'p'
for e-mail or phone, and value is the formatted phone number or e-mail.
The canonical formats are:
     (name, 'p', '###-###-#####')
     (name, 'e', 'someone@something')
If the numbers you submit are formatted differently they will not
match the gold answers

NOTE: ***don't change this interface***, as it will be called directly by
the submit script

NOTE: You shouldn't need to worry about this, but just so you know, the
'f' parameter below will be of type StringIO at submission time. So, make
sure you check the StringIO interface if you do anything really tricky,
though StringIO should support most everything.
"""
def process_file(name, f, debug=False):
    # note that debug info should be printed to stderr
    # sys.stderr.write('[process_file]\tprocessing file: %s\n' % (path))
    res = []
    for line in f:

        ### de-encoding
        # strip attributeless tags
        line = re.sub(r'</?[a-zA-Z_-]+>', '', line)
        # replace entities & url escapes
        line = re.sub(r'&([a-zA-Z])+;', ' ', line)
        line = re.sub(r'&#(x)?([a-zA-Z0-9]+);', lambda m:
                      unichr(int(m.group(2), 10+bool(m.group(1))*6)),
                      line)

        # get phone numbers before we get out bigger guns for email addresses
        res.extend((name, 'p', "-".join(m.groups()))
                   for m in NA_PHONE.finditer(line))

        # nasty hack first, to get rid of "first/last name"@blah.com
        line = re.sub(r'\b(?i)(?:first|last).name.?\s*(?:@|at)', '', line)

        ### deal with javascript
        # stupid obfuscate function hack
        line = re.sub(r'obfuscate[^\w.]+([\w.]+)[^\w.]+([\w.+]+)', r'\2@\1',
                      line)
        # replace numbers with their 'chr'
        line = re.sub(r'(?:\d{2,3},\s*){3,}\d{2,3}',
                      lambda m: "".join(map(unichr, map(int,re.findall('\d+', m.group())))),
                      line)
        # replace 'a'+'b'+... -> ab
        line = re.sub(r'''["']\s*[+]\s*[\"']''', "", line)
        ## de-obfuscation
        # kill NOSPAM
        line = re.sub(r'(?i)(?:n.?[o0].?)?s.?p.?a(.)?m\1?', '', line)
        # remove space-outs: 'a b c @ f o o . c o m' -> 'abc@foo.com'
        line = re.sub(r'(?:[\w@.][^\w@.]){3,}\b', lambda m:re.sub(r'[^\w@.]', '', m.group(0)), line)
        # normalize '@' and '.'
        line = re.sub(r'''(?i)[=\*\(\{\[\"\' ]+(?:at|where)[=\*\)\}\]\"\' ]+''', '@', line)
        line = re.sub(r'''(?i)[=\*\(\{\[\"\' ]+(d[o0]?t)[=\*\)\}\]\"\' ]+''', '.', line)
        line = re.sub(r'(?<![A-Z])(?:AT|WHERE)(?![A-Z])', '@', line)
        line = re.sub(r'(?<![A-Z])(?:D[O0]?[MT])(?![A-Z])', '.', line)
        line = re.sub(r'([@.])+', r'\1', line)
        line = re.sub(r'\s+([@\.])\s+', r'\1', line)
        line = re.sub(r'%([a-zA-Z0-9]{2})', lambda m: unichr(int(m.group(1),16)), line)

        def wordstopunct(m):
            s = m.group().lower()
            name, where = s.split('@', 1)
            if "." not in where:
                where = re.sub(r'\s+', '.', where)
            return name + '@' + where
        line = EMAIL_FIXUP.sub(wordstopunct, line)
        matches = my_first_pat.findall(line)
                                       ## .replace(' WHERE ', '@')
                                       ## .replace(' DOT ', '.')
                                       ## .replace(' dot ', '.')
                                       ## .replace(' dt ', '.')
                                       ## .replace('(at)', '@')
                                       ## .replace(' at ', '@'))
        if debug:
            print >> sys.stderr, "LINE: ", unicode(line).encode('utf-8')
        for m in matches:
            if debug:
                print >> sys.stderr, m
            email = ('%s@%s.%s' % m).lower().replace(';', '.').replace(',', '.')
            if email.startswith('server@'): continue
            res.append((name,'e',email))


    return res

"""
You should not need to edit this function, nor should you alter
its interface as it will be called directly by the submit script
"""
def process_dir(data_path):
    # get candidates
    guess_list = []
    for fname in os.listdir(data_path):
        if fname[0] == '.':
            continue
        path = os.path.join(data_path,fname)
        f = open(path,'r')
        f_guesses = process_file(fname, f)
        guess_list.extend(f_guesses)
    return guess_list

"""
You should not need to edit this function.
Given a path to a tsv file of gold e-mails and phone numbers
this function returns a list of tuples of the canonical form:
(filename, type, value)
"""
def get_gold(gold_path):
    # get gold answers
    gold_list = []
    f_gold = open(gold_path,'r')
    for line in f_gold:
        gold_list.append(tuple(line.strip().split('\t')))
    return gold_list

"""
You should not need to edit this function.
Given a list of guessed contacts and gold contacts, this function
computes the intersection and set differences, to compute the true
positives, false positives and false negatives.  Importantly, it
converts all of the values to lower case before comparing
"""
def score(guess_list, gold_list):
    guess_list = [(fname, _type, value.lower()) for (fname, _type, value) in guess_list]
    gold_list = [(fname, _type, value.lower()) for (fname, _type, value) in gold_list]
    guess_set = set(guess_list)
    gold_set = set(gold_list)

    tp = guess_set.intersection(gold_set)
    fp = guess_set - gold_set
    fn = gold_set - guess_set

    pp = pprint.PrettyPrinter()
    #print 'Guesses (%d): ' % len(guess_set)
    #pp.pprint(guess_set)
    #print 'Gold (%d): ' % len(gold_set)
    #pp.pprint(gold_set)
    print 'True Positives (%d): ' % len(tp)
    pp.pprint(tp)
    print 'False Positives (%d): ' % len(fp)
    pp.pprint(fp)
    print 'False Negatives (%d): ' % len(fn)
    pp.pprint(fn)
    print 'Summary: tp=%d, fp=%d, fn=%d' % (len(tp),len(fp),len(fn))
    return fp, fn

"""
You should not need to edit this function.
It takes in the string path to the data directory and the
gold file
"""
def main(data_path, gold_path, grep):
    guess_list = process_dir(data_path)
    gold_list =  get_gold(gold_path)
    fp, fn = score(guess_list, gold_list)
    if grep:
        import os
        for falsy in [fp, fn]:
            for (fname, t, x) in falsy:
                if t == 'e':
                    match = re.search(r"(?i)(.*)@", x) or re.search('@.*', x)
                    if match:
                        pat = match.group(1)
                        print 'searching for', pat, 'in', fname
                        os.system('fgrep --color=always "%s" -- %s' % (
                            pat, '../data/dev/' + fname))


"""
commandline interface takes a directory name and gold file.
It then processes each file within that directory and extracts any
matching e-mails or phone numbers and compares them to the gold file
"""
if __name__ == '__main__':

    if (len(sys.argv) not in (3, 4)):
        print 'usage:\tSpamLord.py <data_dir> <gold_file>'
        print 'Running self tests'
        tps = [x[-1] for x in process_file('test-fn', POS, debug=1)]
        pprint.pprint(set(ANS)-(set(tps)))
        pprint.pprint(set(tps)-(set(ANS)))
        numfn = len(POS)-len(tps)
        print 'FN:', numfn
        fps = process_file('test-fp', NEG, debug=1)
        pprint.pprint(fps)
        numfp = len(fps)
        print 'FP:', numfp
        assert numfn == numfp == 0

        sys.exit(0)
    main(sys.argv[1],sys.argv[2], sys.argv[3:] == ['grep'])

