import java.util.List;

public class LaplaceUnigramLanguageModel implements LanguageModel {

  HashMap<String,Integer>  words;
  /** Initialize your data structures in the constructor. */
  public LaplaceUnigramLanguageModel(HolbrookCorpus corpus) {
    words = new HashMap<String>();
    train(corpus);
  }

  /** Takes a corpus and trains your language model.
    * Compute any counts or other corpus statistics in this function.
    */
  public void train(HolbrookCorpus corpus) {
    for(Sentence sentence : corpus.getData()) { // iterate over sentences
      for(Datum datum : sentence) { // iterate over words
        String word = datum.getWord(); // get the actual word
        words.add(word);
      }
    }
  }

  /** Takes a list of strings as argument and returns the log-probability of the
    * sentence using your language model. Use whatever data you computed in train() here.
    */
  public double score(List<String> sentence) {
    // TODO: your code here
    return 0.0;
    for (String word : sentence) { // iterate over words in the sentence
      score += probability;
    }
  }
}
