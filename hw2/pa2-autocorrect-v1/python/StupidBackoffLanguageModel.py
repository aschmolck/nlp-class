import math

import UnigramLanguageModel
import LaplaceBigramLanguageModel
import LaplaceUnigramLanguageModel

INF = float('inf')
UNI_WEIGHT = math.log(0.4)
class StupidBackoffLanguageModel:

    def __init__(self, corpus):
        """Initialize your data structures in the constructor."""
        self.laplaceUnigramModel = LaplaceUnigramLanguageModel.LaplaceUnigramLanguageModel(
            corpus, smooth=1)
        ## self.laplaceUnigramModel = UnigramLanguageModel.UnigramLanguageModel(
        ##     corpus)
        self.bigramModel =  LaplaceBigramLanguageModel.LaplaceBigramLanguageModel(
            corpus, smooth=0)

    def train(self, corpus):
        """ Takes a corpus and trains your language model.
            Compute any counts or other corpus statistics in this function.
        """
        self.laplaceUnigramModel.train(corpus)
        self.bigramModel.train(corpus)


    def score(self, sentence):
        """ Takes a list of strings as argument and returns the log-probability of the
            sentence using your language model. Use whatever data you computed in train() here.
        """
        score = 0.0
        for pair in LaplaceBigramLanguageModel.bigrams(sentence):
            score_delta = self.bigramModel.score(pair, conditional=True)
            if score_delta == -INF:
                score_delta = UNI_WEIGHT + self.laplaceUnigramModel.score(pair[1:2])
            score += score_delta

        return score


