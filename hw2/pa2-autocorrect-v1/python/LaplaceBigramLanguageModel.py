import collections
import math
import LaplaceUnigramLanguageModel

# pylint: disable=C0103

def bigrams(seq):
    return (pair for pair in zip(seq, seq[1:]))

class LaplaceBigramLanguageModel:
    def __init__(self, corpus, smooth=1, kneser_ney=False):
        self.smooth = smooth
        self.ngramCounts = collections.defaultdict(int)
        self.total = 0
        self.unigramLanguageModel = LaplaceUnigramLanguageModel.LaplaceUnigramLanguageModel(
            corpus, smooth=0)
        self.train(corpus)

    def train(self, corpus):
        """Takes a HolbrookCorpus corpus, does whatever training is needed."""
        for sentence in corpus.corpus:
            for pair in bigrams(sentence.data):
                self.ngramCounts[tuple(d.word for d in pair)] += 1
                self.total += 1

    def score(self, sentence, conditional=True):
        """Takes a list of strings, returns a score of that sentence."""
        score = 0.0
        smooth = self.smooth
        V = len(self.unigramLanguageModel.ngramCounts)
        for pair in bigrams(sentence):
            count = self.ngramCounts[pair]+smooth
            if not count:
                return float('-inf')
            score += math.log(count)
            C_previousWord = self.unigramLanguageModel.ngramCounts[pair[0]]
            ## print pair, count, pair[0], C_previousWord
            score -= math.log(C_previousWord + smooth*V)
        # pylint: disable=W0631
        if not conditional:
            score += self.unigramLanguageModel.score(pair[-1:])
        return score

if __name__ == '__main__':
    import HolbrookCorpus
    import fractions
    from collections import namedtuple
    import sys
    args = sys.argv[1:]

    smooth = ['--smooth'] == args[:1]
    if smooth: args.pop(0)
    corpus = HolbrookCorpus.HolbrookCorpus()
    corpus.corpus = filter(None, map(corpus.processLine, sys.stdin))
    ## corpus = namedtuple('Corpus', ['corpus'])([line.split() for line in sys.stdin])
    print "\n".join(map(repr, corpus.corpus))
    model = LaplaceBigramLanguageModel(corpus=corpus, smooth=smooth)
    # conditional = don't multiply with p(w_1) = p(<s>) so technically everything is conditioned
    score = model.score(args, conditional=True)
    print "total bigrams: %-10d unigrams: %-10d" % (
        model.total, model.unigramLanguageModel.total)
    print fractions.Fraction(math.exp(score)).limit_denominator()

