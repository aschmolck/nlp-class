import collections
import math

class LaplaceUnigramLanguageModel:

    def __init__(self, corpus, smooth=1):
        self.ngramCounts = collections.defaultdict(int)
        self.total = 0
        self.smooth = smooth
        self.train(corpus)

    def train(self, corpus):
        """Takes a HolbrookCorpus corpus, does whatever training is needed."""
        for sentence in corpus.corpus:
            for datum in sentence.data:
                token = datum.word
                self.ngramCounts[token] += 1
                self.total += 1

    def score(self, sentence):
        """Takes a list of strings, returns a score of that sentence."""
        score = 0.0
        nu = len(self.ngramCounts)
        for token in sentence:
            count = self.ngramCounts[token]
            score += math.log(count + bool(self.smooth))
            score -= math.log(self.total + nu*bool(self.smooth))
        return score
