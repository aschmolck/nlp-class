from __future__ import division
import collections
import math
import LaplaceUnigramLanguageModel
import LaplaceBigramLanguageModel
import StupidBackoffLanguageModel

# pylint: disable=C0103

def bigrams(seq):
    return (pair for pair in zip(seq, seq[1:]))


def trigrams(seq):
    seq = seq[0:1] + seq + seq[-1:]
    return (tri for tri in zip(seq, seq[1:], seq[2:]))

class LaplaceTrigramLanguageModel:
    def __init__(self, corpus, smooth=1):
        self.smooth = smooth
        self.ngramCounts = collections.defaultdict(int)
        self.total = 0
        self.unigramLanguageModel = StupidBackoffLanguageModel.StupidBackoffLanguageModel(
            corpus)
        self.train(corpus)

    def train(self, corpus):
        """Takes a HolbrookCorpus corpus, does whatever training is needed."""
        for sentence in corpus.corpus:
            for pair in trigrams(sentence.data):
                self.ngramCounts[tuple(d.word for d in pair)] += 1
                self.total += 1

    def score(self, sentence):
        """Takes a list of strings, returns a score of that sentence."""
        score = 0.0
        smooth = self.smooth
        for pair in trigrams(sentence):
            count = self.ngramCounts[pair]+smooth
            score += math.log(count) + math.log(0.7) * (
                self.unigramLanguageModel.score(pair[1:]))
        # pylint: disable=W0631
        return score


CustomLanguageModel = LaplaceTrigramLanguageModel
if __name__ == '__main__':
    import HolbrookCorpus
    import fractions
    import sys
    args = sys.argv[1:]

    corpus = HolbrookCorpus.HolbrookCorpus()
    corpus.corpus = filter(None, map(corpus.processLine, sys.stdin))
    ## corpus = namedtuple('Corpus', ['corpus'])([line.split() for line in sys.stdin])
    print "\n".join(map(repr, corpus.corpus))
    model = KneserNeyBigramLanguageModel(corpus=corpus, d=0.75)
    # conditional = don't multiply with p(w_1) = p(<s>) so technically everything is conditioned
    score = model.score(args)
    print "total bigrams: %-10d unigrams: %-10d" % (
        model.total, model.unigramLanguageModel.total)
    print fractions.Fraction(math.exp(score)).limit_denominator()

