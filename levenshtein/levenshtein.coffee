dmat = d3.select('.distance-matrix')

add_letterboxes = (g, W) ->
  arrows = '←↑→↓↖↗↘↙'.split('')
  W = 40
  x_0 = W*3
  y_0 = W*3
  g.append('rect').attr('height', W).attr('width', W)
                  .attr('stroke', 'black').attr('stroke-width', 1).attr('fill', 'white')
                  .attr('x', ([[_,j],d]) -> x_0 + j*W)
                  .attr('y', ([[i,_],d]) -> y_0 + i*W)
                  .attr('fill', ([[i,j],_]) ->
                                 if i == j == -1 then 'white'
                                 else if i == -1 or j == -1 then 'darkslateblue'
                                 else 'white')
                  .attr('style', (_,i) -> 'opacity:' + ~~i)
  g.append('text').attr('class', 'letter')
                  .attr('x', ([[_,j],d]) -> x_0 + (j+0.5)*W)
                  .attr('y', ([[i,_],d]) -> y_0 + (i+0.5)*W)
                  .text(([_,d])->d);
#  g.append('g').selectAll('text.arrow')
#                  .data((_,i) -> arrows.map((a)->[a,i])).enter()
#                  .append('text')
#                      .attr('class', 'arrow')
#                      .attr('x', W/2)
#                      .attr('y', ([a,i]) -> (i+0.5)*W).text(([a,_])->a);
#  g.append('path').attr('style', 'stroke: red')
#                  .attr('d', ([[i,j],_]) ->
#                    'M '.concat(x_0 + j*W, ' ', y_0 + (i+0.5)*W, ' h ', W))


srep = (s, n) ->
    (s for i in [0...n]).join("")

pad = (s, c, n) ->
    srep(c, Math.max(0, n-s.length)) + s

class Mat extends Array
  constructor: (@m, @n) ->
    this.splice(0, @m*@n-1, new Array(@m*@n))
  at: (i,j) ->
    return this[i*@n + j]
  setTo: (v, i,j) ->
    this[i*@n + j] = v
  toString: ->
    width = Math.max.apply(self.map (x) -> x.toString().length)
    for i in [0...@m]
      row = ",".join(pad(@at(i,j).toString(), ' ', width) for j in [0...@n])
      s += (if i == 0 then " " else "[") + row + (if i == @m-1 then "]" else ";\n")
    s

draw_matrix = () ->
  from = "INTENTION"
  to = "EXECUTION"
  W = 50;
  fromLetters = ("ø"+from).split("")
  toLetters = ("ø"+to).split("")
  m = toLetters.length;
  n = fromLetters.length;
  fields = new Array (m+1)*(n+1)
  for i in [0...m+1]
    for j in [0...n+1]
      fields[i * (m + 1) + j] = [[i - 1, j - 1], if i == 0 and j > 0
                                  fromLetters[j-1]
                                else if j == 0 and i > 0
                                  toLetters[i-1]
                                else
                                  null]
  g=dmat.selectAll('g.letter').data(fields).enter().append('g');
  add_letterboxes(g, W)
  fields[4] = fields[2]
  add_letterboxes(g.data(fields), W)

draw_matrix()