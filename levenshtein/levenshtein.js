(function() {
  var Mat, add_letterboxes, dmat, draw_matrix, pad, srep,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  dmat = d3.select('.distance-matrix');

  add_letterboxes = function(g, W) {
    var arrows, x_0, y_0;
    arrows = '←↑→↓↖↗↘↙'.split('');
    W = 40;
    x_0 = W * 3;
    y_0 = W * 3;
    g.append('rect').attr('height', W).attr('width', W).attr('stroke', 'black').attr('stroke-width', 1).attr('fill', 'white').attr('x', function(_arg) {
      var d, j, _, _ref;
      (_ref = _arg[0], _ = _ref[0], j = _ref[1]), d = _arg[1];
      return x_0 + j * W;
    }).attr('y', function(_arg) {
      var d, i, _, _ref;
      (_ref = _arg[0], i = _ref[0], _ = _ref[1]), d = _arg[1];
      return y_0 + i * W;
    }).attr('fill', function(_arg) {
      var i, j, _, _ref;
      (_ref = _arg[0], i = _ref[0], j = _ref[1]), _ = _arg[1];
      if ((i === j && j === -1)) {
        return 'white';
      } else if (i === -1 || j === -1) {
        return 'darkslateblue';
      } else {
        return 'white';
      }
    }).attr('style', function(_, i) {
      return 'opacity:' + ~~i;
    });
    return g.append('text').attr('class', 'letter').attr('x', function(_arg) {
      var d, j, _, _ref;
      (_ref = _arg[0], _ = _ref[0], j = _ref[1]), d = _arg[1];
      return x_0 + (j + 0.5) * W;
    }).attr('y', function(_arg) {
      var d, i, _, _ref;
      (_ref = _arg[0], i = _ref[0], _ = _ref[1]), d = _arg[1];
      return y_0 + (i + 0.5) * W;
    }).text(function(_arg) {
      var d, _;
      _ = _arg[0], d = _arg[1];
      return d;
    });
  };

  srep = function(s, n) {
    var i;
    return ((function() {
      var _results;
      _results = [];
      for (i = 0; 0 <= n ? i < n : i > n; 0 <= n ? i++ : i--) {
        _results.push(s);
      }
      return _results;
    })()).join("");
  };

  pad = function(s, c, n) {
    return srep(c, Math.max(0, n - s.length)) + s;
  };

  Mat = (function(_super) {

    __extends(Mat, _super);

    function Mat(m, n) {
      this.m = m;
      this.n = n;
      this.splice(0, this.m * this.n - 1, new Array(this.m * this.n));
    }

    Mat.prototype.at = function(i, j) {
      return this[i * this.n + j];
    };

    Mat.prototype.setTo = function(v, i, j) {
      return this[i * this.n + j] = v;
    };

    Mat.prototype.toString = function() {
      var i, j, row, width, _ref;
      width = Math.max.apply(self.map(function(x) {
        return x.toString().length;
      }));
      for (i = 0, _ref = this.m; 0 <= _ref ? i < _ref : i > _ref; 0 <= _ref ? i++ : i--) {
        row = ",".join((function() {
          var _ref2, _results;
          _results = [];
          for (j = 0, _ref2 = this.n; 0 <= _ref2 ? j < _ref2 : j > _ref2; 0 <= _ref2 ? j++ : j--) {
            _results.push(pad(this.at(i, j).toString(), ' ', width));
          }
          return _results;
        }).call(this));
        s += (i === 0 ? " " : "[") + row + (i === this.m - 1 ? "]" : ";\n");
      }
      return s;
    };

    return Mat;

  })(Array);

  draw_matrix = function() {
    var W, fields, from, fromLetters, g, i, j, m, n, to, toLetters, _ref, _ref2;
    from = "INTENTION";
    to = "EXECUTION";
    W = 50;
    fromLetters = ("ø" + from).split("");
    toLetters = ("ø" + to).split("");
    m = toLetters.length;
    n = fromLetters.length;
    fields = new Array((m + 1) * (n + 1));
    for (i = 0, _ref = m + 1; 0 <= _ref ? i < _ref : i > _ref; 0 <= _ref ? i++ : i--) {
      for (j = 0, _ref2 = n + 1; 0 <= _ref2 ? j < _ref2 : j > _ref2; 0 <= _ref2 ? j++ : j--) {
        fields[i * (m + 1) + j] = [[i - 1, j - 1], i === 0 && j > 0 ? fromLetters[j - 1] : j === 0 && i > 0 ? toLetters[i - 1] : null];
      }
    }
    g = dmat.selectAll('g.letter').data(fields).enter().append('g');
    add_letterboxes(g, W);
    fields[4] = fields[2];
    return add_letterboxes(g.data(fields), W);
  };

  draw_matrix();

}).call(this);
